export interface Employee{
    id?: string;
    nombre: string;
    apellido: string;
    email: string;
    fecha: string;
    cedula: string;
    celular: string;
    departamento: string;
    ciudad: string;
    barrio: string;
    direccion: string;
    salario: string;
    otrosin: string;
    gastosmen: string;
    gastosfin: string;
}