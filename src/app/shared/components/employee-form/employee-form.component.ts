import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { error } from 'selenium-webdriver';
import { EmployeesService } from 'src/app/pages/employees/employees.service';
import { Employee } from '../header/models/employee.intarface';


@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {

  value: Employee;
  employeeForm: FormGroup;
  private isEmail = /\S+@\S+\.\S+/;
  constructor(private router: Router, private fb: FormBuilder, private employeesSvc: EmployeesService) { 
    const navigation = this.router.getCurrentNavigation();
    this.value = navigation?.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if(typeof this.value === 'undefined'){
      this.router.navigate(['new']);
    }else{
      this.employeeForm.patchValue(this.value);
    }
  }

  onSave(): void{
    console.log('Se ha guardado', this.employeeForm.value)
    if(this.employeeForm.valid){
      const employee = this.employeeForm.value;
      const employeeId = this.value?.id || null;
      this.employeesSvc.onSaveEmployee(employee, employeeId);
      this.employeeForm.reset();
    }else{
      console.log('Error Formulario');
      
    }
  }

  onGoToBackToList(): void{
    this.router.navigate(['list'])
  }

  isValidField(field: string): string{
    const validatedField = this.employeeForm.get(field);
    return ( !validatedField.valid && validatedField.touched) ? 'Es inválido' : validatedField.touched ? 'Es Válido' : '';
  }

  private initForm(): void{
    this.employeeForm = this.fb.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.isEmail)]],
      fecha: ['', [Validators.required]],

      cedula: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      barrio: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      salario: ['', [Validators.required]],
      otrosin: ['', [Validators.required]],
      gastosmen: ['', [Validators.required]],
      gastosfin: ['', [Validators.required]]
    })
  }
}
