import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Employee } from 'src/app/shared/components/header/models/employee.intarface';
import { EmployeesService } from '../employees.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  value: Employee = null;

  navigationExtras: NavigationExtras = {
    state:{
      value:null
    }
  };
  
  constructor(private router: Router, private employeesSvs: EmployeesService) { 
    const navigation = this.router.getCurrentNavigation();
    this.value = navigation?.extras?.state?.value;
  }

  ngOnInit(): void {
    if(typeof this.value === 'undefined'){
      this.router.navigate(['list']);
    }
  }

  onGoToEdit(): void{
    this.navigationExtras.state.value = this.value;
    this.router.navigate(['edit'], this.navigationExtras);
  }

  async onDelete(): Promise<void>{
    try {
      await this.employeesSvs.onDeleteEmployee(this.value?.id);
      alert('Se ha eliminado');
      this.onGoToBackToList();
    } catch (error) {
      console.log(error)
    }
  }
  
  onGoToBackToList(): void{
    this.router.navigate(['list']);
  }
}
