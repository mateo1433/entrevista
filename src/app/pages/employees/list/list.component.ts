import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Employee } from 'src/app/shared/components/header/models/employee.intarface';
import { EmployeesService } from '../employees.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  employees$ = this.employeesSvs.employees;
  navigationExtras: NavigationExtras = {
    state:{
      value:null
    }
  }



  constructor(private router: Router, private employeesSvs: EmployeesService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item: any): void{
    this.navigationExtras.state.value = item;
    this.router.navigate(['edit'], this.navigationExtras);
  }

  onGoToSee(item: any): void{
    this.navigationExtras.state.value = item;
    this.router.navigate(['details'], this.navigationExtras);
  }

  async onGoToDelete(empId: string): Promise<void>{
    try {
      await this.employeesSvs.onDeleteEmployee(empId);
      alert('Se ha eliminado');
    } catch (error) {
      console.log(error)
    }
    
  }

}
