// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAMnrQUnxuBv3OfT9Pr7BVaUrdeKExmLgc",
    authDomain: "panel-consulta.firebaseapp.com",
    projectId: "panel-consulta",
    storageBucket: "panel-consulta.appspot.com",
    messagingSenderId: "315301355625",
    appId: "1:315301355625:web:3839c3ec052ce5ce3f82f8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
